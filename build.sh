#!/bin/bash

URL="registry.gitlab.com/fabsor_/ubuntu-apache-base"
if [ ! $1 ]; then
    tag=$(git rev-parse --abbrev-ref HEAD)
else
    tag=$1
fi


docker build -t $URL .
docker build -t $URL:$tag .
docker push $URL:$tag
