FROM ubuntu:xenial
RUN apt-get update && apt-get -y install locales php7.0 apache2 libapache2-mod-php7.0  mcrypt php7.0-mcrypt php-mbstring php-pear php7.0-dev php7.0-xml apt-transport-https composer php7.0-zip php7.0-mysql php7.0-soap
RUN echo "sv_SE.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen
RUN rm /etc/apache2/mods-enabled/*
RUN a2enmod -q access_compat
RUN a2enmod -q alias
RUN a2enmod -q authn_core
RUN a2enmod -q authz_core
RUN a2enmod -q authz_host
RUN a2enmod -q deflate
RUN a2enmod -q php7.0
RUN a2enmod -q dir
RUN a2enmod -q env
RUN a2enmod -q expires
RUN a2enmod -q filter
RUN a2enmod -q headers
RUN a2enmod -q mime
RUN a2enmod -q mpm_prefork
RUN a2enmod -q rewrite
RUN a2enmod -q setenvif
RUN a2enmod -q socache_shmcb
RUN rm /etc/apache2/conf-enabled/*
COPY templates/performance.conf /etc/apache2/conf-available/performance.conf
COPY templates/security.conf /etc/apache2/conf-available/security.conf
COPY templates/vhost.conf /etc/apache2/sites-enabled/vhost.conf
RUN a2enconf -q performance
RUN a2enconf -q security
RUN a2enconf -q other-vhosts-access-log.conf
RUN a2dissite -q 000-default
RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log && ln -sf /proc/self/fd/1 /var/log/apache2/error.log && ln -sf /proc/self/fd/1 /var/log/apache2/other_vhosts_access.log
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
RUN mkdir -p /srv/www/app
RUN mkdir /srv/www/app/log
RUN mkdir /srv/www/app/web
WORKDIR /srv/www/app
ENV PHPDEV 0
EXPOSE 80
CMD /usr/sbin/apache2ctl -D FOREGROUND
